import javax.crypto.spec.*;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Random;

public class DH{
	
	private static 		BigInteger	g,p;
	private static final 	int 	bitStrength 	= 4096;

	private static			IvParameterSpec ivspec;
	
	public					int 	prvKey;
	
	//For debugging
	/*public static void main(String[] args){
		
		BigInteger key1,key2,shrdkey1,shrdkey2;
		
		EnDeCrypt encryption = new EnDeCrypt();
		System.out.println(encryption.getPrivateKey());
		System.out.println(encryption.getPrime());
		//encryption.setG(BigInteger.valueOf(5));
		System.out.println(encryption.getG());
		
		
		EnDeCrypt encryption2 = new EnDeCrypt();
		encryption2.setG(BigInteger.valueOf(5));
		System.out.println(encryption2.getPrivateKey());
		System.out.println(encryption2.getPrime());
		System.out.println(encryption2.getG());
		
		//key1=calcKey(encryption.getG(),BigInteger.valueOf(encryption.getPrivateKey()),encryption.getPrime());
		//key2=calcKey(encryption2.getG(),BigInteger.valueOf(encryption2.getPrivateKey()),encryption2.getPrime());
		
		//key1=calcKey(BigInteger.valueOf(5),BigInteger.valueOf(encryption.getPrivateKey()),encryption.getPrime());
		//key2=calcKey(BigInteger.valueOf(5),BigInteger.valueOf(encryption2.getPrivateKey()),encryption2.getPrime());
		
		System.out.println("k1:" + key1);
		System.out.println("k2:" + key2);
		
		//shrdkey1 = calcKey(key1,BigInteger.valueOf(encryption.getPrivateKey()),encryption.getPrime());
		//shrdkey2 = calcKey(key2,BigInteger.valueOf(encryption2.getPrivateKey()),encryption.getPrime());
		
		shrdkey1 = calcKey(key2,BigInteger.valueOf(encryption.getPrivateKey()),encryption.getPrime());
		shrdkey2 = calcKey(key1,BigInteger.valueOf(encryption2.getPrivateKey()),encryption.getPrime());
		
		System.out.println("shrdk1:" + shrdkey1);
		System.out.println("shrdk2:" + shrdkey2);
	}*/
	
	public DH(){
		
		setPrime();
		setG();
		setPrivateKey();
	}

	public static BigInteger calcKey(BigInteger a,BigInteger b, BigInteger p){
		
		BigInteger i;
		
		i=a.modPow(b, p);
			
		return i;
	}
	
	public BigInteger getG(){
		
		return g;
		
	}
	private void setG(){			
		
		/*//Random rnd = new Random();
		SecureRandom rnd = new SecureRandom();
		
		g=BigInteger.probablePrime(bitStrength/2, rnd);
		
		while(!(g.gcd(p).equals(BigInteger.ONE))){
			System.out.println("Not coprime!");
			g.add(BigInteger.ONE);
		}*/
		
		g=BigInteger.valueOf(2);
			
	}
	
	public void setG(BigInteger puG){
		
		g=puG;
		
	}
	
	public IvParameterSpec getIVSpec(){
		return ivspec;
	}
	
	public BigInteger getPrime(){
		
		return p;
	}
	private static void setPrime(){
		
		String s="FFFFFFFFFFFFFFFFC90FDAA22168C234C4C6628B80DC1CD1"
				+"29024E088A67CC74020BBEA63B139B22514A08798E3404DD"
				+"EF9519B3CD3A431B302B0A6DF25F14374FE1356D6D51C245"
				+"E485B576625E7EC6F44C42E9A637ED6B0BFF5CB6F406B7ED"
				+"EE386BFB5A899FA5AE9F24117C4B1FE649286651ECE45B3D"
				+"C2007CB8A163BF0598DA48361C55D39A69163FA8FD24CF5F"
				+"83655D23DCA3AD961C62F356208552BB9ED529077096966D"
				+"670C354E4ABC9804F1746C08CA18217C32905E462E36CE3B"
				+"E39E772C180E86039B2783A2EC07A28FB5C55DF06F4C52C9"
				+"DE2BCBF6955817183995497CEA956AE515D2261898FA0510"
				+"15728E5A8AAAC42DAD33170D04507A33A85521ABDF1CBA64"
				+"ECFB850458DBEF0A8AEA71575D060C7DB3970F85A6E1E4C7"
				+"ABF5AE8CDB0933D71E8C94E04A25619DCEE3D2261AD2EE6B"
				+"F12FFA06D98A0864D87602733EC86A64521F2B18177B200C"
				+"BBE117577A615D6C770988C0BAD946E208E24FA074E5AB31"
				+"43DB5BFCE0FD108E4B82D120A92108011A723C12A787E6D7"
				+"88719A10BDBA5B2699C327186AF4E23C1A946834B6150BDA"
				+"2583E9CA2AD44CE8DBBBC2DB04DE8EF92E8EFC141FBECAA6"
				+"287C59474E6BC05D99B2964FA090C3A2233BA186515BE7ED"
				+"1F612970CEE2D7AFB81BDD762170481CD0069127D5B05AA9"
				+"93B4EA988D8FDDC186FFB7DC90A6C08F4DF435C934063199"
				+"FFFFFFFFFFFFFFFF";
		
		p=new BigInteger(s,16);
	}
	
	private void setPrivateKey(){

		Random rnd = new Random();
		
		prvKey = rnd.nextInt(Integer.MAX_VALUE);
		
	}
	
	public void setPrivateKey(int i){

		prvKey = i;
		
	}
	
	public int getPrivateKey(){
		
		return prvKey;
		
	}
	
	
	/*public static String AESKey(BigInteger shrdKey){
		
		byte[] key;
		String msg = null;
		
		try {
			key=hashDigest(shrdKey);
			msg=new BigInteger(key).toString(16);
			
			
		} catch (NoSuchAlgorithmException e1) {
			e1.printStackTrace();
		}
		
		return msg.substring(0, 16);
		
	}*/
	
}