import java.math.BigInteger;
import java.net.*;
import java.util.Scanner;
import java.io.*;

public class Client {

	final static int port = 1337;
	private BufferedReader input;
	private PrintWriter output;
	
	public static void main(String[] args) throws IOException {
		Client client = new Client();
		client.connectToServer();
	}
	
	public void connectToServer() throws IOException{
		
		BigInteger 		shrdKey = null, sessKey=null, PuA;
		DH 		crypto = null;
		InetAddress 	address = InetAddress.getLoopbackAddress();
		String 			server = address.getHostAddress();
		Socket 			socket = null;
		
		try {
			socket = new Socket(server,port);
			System.out.println("Connected to server: "+server);
			
			input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			output = new PrintWriter(socket.getOutputStream(),true);
			
			crypto = new DH();
			
			crypto.setG(new BigInteger(input.readLine()));
			PuA=new BigInteger(input.readLine());
			sessKey=DH.calcKey(crypto.getG(),BigInteger.valueOf(crypto.getPrivateKey()),crypto.getPrime());
			output.println(sessKey);
			shrdKey=DH.calcKey(PuA, BigInteger.valueOf(crypto.getPrivateKey()), crypto.getPrime());
			
			AES aes = new AES(shrdKey.toString());
		} 
		catch (ConnectException ce)	{ System.out.println("Cannot connect to server. Exiting."); System.exit(0);		}
		catch (IOException e) 		{ e.printStackTrace();		}
		
		while(true){
			System.out.println("Input text");
			
			String msg = null,in = null;
			Scanner kbrd = new Scanner(System.in);
			
			in = new String(kbrd.nextLine());
			
			if(in.equalsIgnoreCase("exit")){
				kbrd.close();
				input.close();
				output.close();
				socket.close();
				System.exit(1);
			}
			else if(!in.isEmpty()){
				
				//System.out.println(AES.getKey());
				
				AES.encrypt(in);
				msg = AES.getEncryptedString();
				System.out.println("Encrypted msg: " + msg);
				output.println(msg);
				
				try {
					in = input.readLine();
					AES.decrypt(in);
					msg=AES.getDecryptedString();
					System.out.println(msg);
				} 
				catch (IOException e) { System.out.println("No message read");}
			}	
		}
	}
}
