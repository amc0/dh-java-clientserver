import java.io.*;
import java.math.BigInteger;
import java.net.*;

public class Server {

	final static int port = 1337;
	
	public static void main(String[] args) throws IOException{
		
		System.out.println("Server started.");
		ServerSocket serverListener = new ServerSocket(port);
		try{		new CryptoServer(serverListener.accept()).run();	}
		finally{	serverListener.close();								}
	}

	private static class CryptoServer extends Thread{
		
		private BufferedReader input;
		private PrintWriter output;
		private Socket socket;
		
		
		public CryptoServer(Socket socket){
			this.socket = socket;
			System.out.println("Client connected");
		}
		
		public void run(){
			
			BigInteger shrdKey = null,PuB,sessKey=null;
			DH crypto = null;
			
			try{
				input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				output = new PrintWriter(socket.getOutputStream(),true);
				
				crypto = new DH();

				output.println(crypto.getG());
				sessKey=DH.calcKey(crypto.getG(),BigInteger.valueOf(crypto.getPrivateKey()),crypto.getPrime());
				output.println(sessKey);
				PuB=new BigInteger(input.readLine());
				shrdKey=DH.calcKey(PuB, BigInteger.valueOf(crypto.getPrivateKey()), crypto.getPrime());
				
				AES aes = new AES(shrdKey.toString());
				
				while(true){
					String in=input.readLine();
					String msg;

					if(in==null)	break;
					
					//System.out.println(in);
					
					AES.decrypt(in);
					msg=AES.getDecryptedString();
					
					System.out.println("Decrypted msg: " +msg);
					msg=("Recieved message: "+msg);
					AES.encrypt(msg);
					
					output.println(AES.getEncryptedString());
				}
			}
			catch(SocketException sx){	try {input.close();	}
										catch (IOException e) { e.printStackTrace();} 
										output.close(); 
										try { socket.close();} 
										catch (IOException e) {	e.printStackTrace();}
									}
			catch(IOException ex){	System.out.println("Problem with " + ex);	}
			finally{				System.out.println("Done transmitting!");
				try{				socket.close();								} 
				catch (IOException e) {	System.out.println(e);					}
			}
		}	
	}	
}

