import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
/**
Aes encryption
*/
public class AES
{
    
    private static SecretKeySpec secretKey ;
    private static byte[] key ;
    
    private static String decryptedString;
    private static String encryptedString;
    
	private static final 	String 	cipherInstance 	= "AES/CBC/PKCS5Padding";
    
	public AES(String key){
		setKey(key);
	}
	
	public static SecretKeySpec getKey(){
		return secretKey;
	}
	
    public static void setKey(String myKey){
        
   
        MessageDigest sha = null;
        try {
            key = myKey.getBytes("UTF-8");
            sha = MessageDigest.getInstance("SHA-1");
            key = sha.digest(key);
            key = Arrays.copyOf(key, 16); // use only first 128 bit
            secretKey = new SecretKeySpec(key, "AES");
            
            
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
    
    public static String getDecryptedString() {
        return decryptedString;
    }
    public static void setDecryptedString(String decryptedString) {
        AES.decryptedString = decryptedString;
    }
    public static String getEncryptedString() {
        return encryptedString;
    }
    public static void setEncryptedString(String encryptedString) {
        AES.encryptedString = encryptedString;
    }
    
    private static IvParameterSpec getIVSpec(){
    	
    	byte[]		iv				= {1,8,2,8,3,4,4,7,8,3,6,4,15,5,14,11};
		IvParameterSpec ivspec = new IvParameterSpec(iv);
    	
		return ivspec;
    }
    
    public static String encrypt(String strToEncrypt)
    {
        try
        {
            Cipher cipher = Cipher.getInstance(cipherInstance);
        
            cipher.init(Cipher.ENCRYPT_MODE, secretKey,getIVSpec());
            byte[] encryptedValue = cipher.doFinal(strToEncrypt.getBytes("UTF-8"));
            byte[] encodedValue = new Base64().encode(encryptedValue);
            
            setEncryptedString(new String(encodedValue));
        }
        catch (Exception e)
        {
           
            System.out.println("Error while encrypting: "+e.toString());
        }
        return null;
    }
    public static String decrypt(String strToDecrypt)
    {
		
        try
        {
            Cipher cipher = Cipher.getInstance(cipherInstance);
           
            cipher.init(Cipher.DECRYPT_MODE, secretKey,AES.getIVSpec());
            byte[] decodeValue = new Base64().decode(strToDecrypt.getBytes());
            byte[] decryptedVal = cipher.doFinal(decodeValue);
            
            setDecryptedString(new String(decryptedVal));
        }
        catch (Exception e)
        {
         
            System.out.println("Error while decrypting: "+e.toString());
        }
        return null;
    }
    
    public static void main(String args[])
    {
                final String strToEncrypt = "My text to encrypt";
                final String strPssword = "encryptor key";
                AES.setKey(strPssword);
               
                AES.encrypt(strToEncrypt.trim());
           
                final String strToDecrypt =  AES.getEncryptedString();
                AES.decrypt(strToDecrypt.trim());
        
    }
     
}